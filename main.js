class Transformer {
    constructor(name, health = 100) {
        this.name = name;
        this.health = health;
    }
    attack() {
    }
    hit(weapon) {
        
        this.health -= weapon.damage;
    }

}
class AutoBot extends Transformer {
    constructor(name, weapon) {
        super(name);
        this.weapon = weapon;
    }
    attack() {
        return this.weapon.fight();
    }
}
class Decepticon extends Transformer {
    attack() {
        return {damage: 5, speed: 1000}
    }
}
class Weapon {
    constructor(damage, speed) {
        this.damage = damage;
        this.speed = speed;
    }
    fight() {
        return {damage: this.damage, speed: this.speed}
    }
}
const transformer1 = new AutoBot('OptimusPrime', new Weapon(100, 1000));
console.log(transformer1);
const transformer2 = new Decepticon('Megatron', 10000);
console.log(transformer2);
class Arena {
    constructor(side1, side2, visualizer) {
        this.side1 = side1;
        this.side2 = side2;
        this.visualizer = visualizer;
    }
    startFightWithNewStrategy() {
        const renderInterval = setInterval(() => {
            this.visualizer.render(this.side1.map(bot => bot.health), this.side2.map(bot => bot.health));
            if (!this.side1.length || !this.side2.length) {
                console.log(`Таймер рендеринга ${renderInterval} очистился !`)
                clearInterval(renderInterval);
            }
        }, 0);
        this.fight(this.side1, this.side2);
        this.fight(this.side2, this.side1);
    }
    findMaxHealthBot(side){
        let maxHealthBot=side[0];
        side.forEach(element=>{
            if(maxHealthBot.health<element.health){
                maxHealthBot=element;
            }
        })
        return maxHealthBot;
    }
    fight(s1, s2) {

        s1.forEach(bot => {
            const interval = setInterval(() => {
                if (!bot || !bot.health || bot.health < 0) {
                    console.log(`Таймер ${interval} очистился ! Бот мертв`);
                    clearInterval(interval);
                    return;
                }
                if (!s2.length) {
                    console.log(`Таймер ${interval} очистился ! Победа`)
                    clearInterval(interval);
                    return;
                }
                const botFromSide2 = this.findMaxHealthBot(s2);
                if (botFromSide2) {
                    botFromSide2.hit(bot.attack());
                    console.log(`Атака бота ${bot?.name} - ${bot?.health}, здоровье противника ${botFromSide2.name} - ${botFromSide2.health}`, bot.attack().speed);
                    if (s2 === this.side1) {
                        s2 = this.side1 = s2.filter(bot => bot.health > 0);
                    }
                    if (s2 === this.side2) {
                        s2 = this.side2 = s2.filter(bot => bot.health > 0);
                    }
                }
            }, bot.attack().speed) 
        });
    }
}


class Visualizer {
    constructor() {
    }
    render(healthSide1,healthSide2){
        const side1El=document.querySelector('.js-arena-side-1');
        const side2El=document.querySelector('.js-arena-side-2');
        side1El.innerHTML=healthSide1.map(health=> `<div class='bot'><progress value="${health}" max="100"></progress></div>`);
        side2El.innerHTML=healthSide2.map(health=> `<div class='bot'><progress value="${health}" max="10000"></progress></div>`);
    }
}
const area = new Arena(
    [
        new AutoBot('Bot1', new Weapon(100, 1000)),
        new AutoBot('Bot2', new Weapon(100, 3000)),
        new AutoBot('Bot3', new Weapon(100, 2000)),
        new AutoBot('Bot4', new Weapon(100, 500)),
    ],
    [
        new Decepticon('Metatron', 10000)
    ],
    new Visualizer()
);
area.startFightWithNewStrategy();






